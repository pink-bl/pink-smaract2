#!../../bin/linux-x86_64/mcs2

## You may have to change mcs2 to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/mcs2.dbd"
mcs2_registerRecordDeviceDriver pdbbase

## test
epicsEnvSet("IOCBL", "PINK")
epicsEnvSet("IOCDEV", "SMA01")
epicsEnvSet("DEVIP", "172.17.10.85")
epicsEnvSet("DEVPORT", "55551")

## device setup
drvAsynIPPortConfigure("SMA", "$(DEVIP):$(DEVPORT)", 0, 0, 0)

# PORT, MCS_PORT, number of axes, active poll period (ms), idle poll period (ms)
#MCS2CreateController("MCS2", "SMA", 12, 250, 1000)
MCS2CreateController("MCS2", "SMA", 2, 250, 1000)

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")

cd "${TOP}/iocBoot/${IOC}"

dbLoadTemplate("motor.substitutions_2", "BL=$(IOCBL),DEV=$(IOCDEV)")

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "P=$(IOCBL):$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
